document.addEventListener("touchstart", function () {
}, false);

$(document).ready(function () {

  var overlay = $('.overlay');
  var modal = $('.modal');

  function center() {
    var height = modal.find('.part').outerHeight();
    modal.innerHeight(height);
  }

  function closeModal() {
    overlay.hide();
    modal.hide();
    enableScrolling();
  }

  function openModal() {
    getQuote();
  }

  $(document).on('click', '.navbar-brand', function (event) {
    event.preventDefault();
    openModal();
  });

  $(document).on('keyup', function (event) {
    if (event.keyCode == 27) {        // esc
      closeModal();
    }
  });
  $(document).on('click', '.overlay', function () {
    closeModal();
  });
  $(document).on('click', '.close-modal', function () {
    closeModal();
  });

  function getQuote() {
    $.ajax({
      url: '/get-quote/',
      success: function (result) {
        var modal = $('.modal');
        var txt = modal.find('.text');
        var author = modal.find('.author');
        txt.html('"' + result['text'] + '"');
        author.html('— ' + result['author']);
        overlay.show();
        modal.show();
        center();
        disableScrolling();
      }
    });
  }

});

function disableScrolling() {
  $('body').addClass('stop-scrolling');
  $('body').bind('touchmove', function (e) {
    e.preventDefault();
  });
}

function enableScrolling() {
  $('body').removeClass('stop-scrolling');
  $('body').unbind('touchmove');
}
