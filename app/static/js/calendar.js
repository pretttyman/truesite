var DIFFERENCE = 12; // It's magic const, that decrease textarea height
var MAX_HEIGHT = 160;
var no_tasks = '<h4 class="info"></h4>'
var no_completed = "There aren't completed tasks";
var no_your = "You haven't any tasks yet";

var thing_tag = '<div class="thing"> \
                   <div class="btn-group"> \
                     <button class="show btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button> \
                     <button class="complete btn btn-default btn-xs"><span class="glyphicon glyphicon-ok"></button> \
                     <button class="delete btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></button> \
                   </div> \
                   <div class="text"></div> \
                 </div>';

var done_tag = '<div class="thing"> \
                   <div class="btn-group"> \
                     <button class="delete btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></button> \
                   </div> \
                   <div class="text"></div> \
                 </div>';

var count_tasks = 0;

function resize(textarea) {
  $(textarea).css({
    'height': 'auto',
    'overflow-y': 'hidden scroll'
  }).height(Math.min(MAX_HEIGHT, textarea.scrollHeight - DIFFERENCE));
  rightListResize();
}

$(document).on('input', 'textarea', function () {
  resize(this);
});

function resizeBlock(block) {
  var margin = block.outerHeight(true) - block.outerHeight();
  block.outerHeight(block.outerHeight() - margin);
}

$(document).ready(function () {
  resizeBlock($('#left'));
  resizeBlock($('#right'));
  rightListResize();
  leftListResize();
});

$(window).resize(function () {
  rightListResize();
  leftListResize();
});

function rightListResize() {
  var t_list = $('#task-list');
  var block = t_list.parent();
  var height = -block.height();
  $('#right').find('.part').each(function () {
    height += $(this).outerHeight(true);
  });
  height += $('#left h4').outerHeight(true);
  t_list.outerHeight($('#right').height() - height);
}

function leftListResize() {
  var list = $('#done-tasks');
  var block = list.parent();
  var height = -block.height();
  $('#left').find('.part').each(function () {
    height += $(this).outerHeight(true);
  });
  height += $('#chosen-date').outerHeight(true);
  list.outerHeight($('#left').height() - height);
}

$(document).ready(function () {
  $(document).on('submit', '#new-task', function (e) {
    e.preventDefault();
    var button = $(this).find(':submit');
    button.attr({disabled: 'disabled'});
    $(this['task']).val(normalizeText($(this['task']).val()));
    var data = $(this).serialize();
    $.ajax({
      type: 'POST',
      url: '/add-task/',
      data: data,
      success: function (result) {
        button.removeAttr('disabled');
        addTask($('#task-list  .list-group'), result['id'], result['task'], thing_tag);
      }
    });
    var textarea = $(this).find('textarea');
    textarea.val('');
    resize(textarea);
  });

  addInfo($('#task-list'), no_your);

});

function addInfo(list, messsage) {
  if (list.find('.thing').size() == 0 && list.find('.info').size() == 0) {
    var info = $(no_tasks);
    list.append(info);
    info.html(messsage);
  }
}

function addTask(list, id, content, tag) {
  if (content == '') {
    return;
  }
  changed_stats = true;
  list.parent().find('.info').remove();
  var task = $(tag);
  list.append(task);
  task.find('.text').html(linkify(content));
  task.attr('id', 'task-' + id);
}

function showDoneTasks(tasks) {
  var done_tasks = $('#done-tasks .list-group');
  done_tasks.html('');
  if (tasks.length == 0) {
    addInfo($('#done-tasks'), no_completed);
  }
  else {
    for (var i = 0; i < tasks.length; ++i) {
      addTask(done_tasks, tasks[i]['id'], tasks[i]['content'], done_tag);
    }
  }
}

function getDoneTasks(date) {
  $.ajax({
    type: 'POST',
    url: '/get-done-tasks/',
    data: date,
    dataType: 'json',
    success: function (result) {
      showDoneTasks(result['tasks']);
    }
  });
}

function deleteTask(id_task) {
  changed_stats = true;
  addInfo($('#task-list'), no_your);
  addInfo($('#done-tasks'), no_completed);
  $.ajax({
    type: 'POST',
    url: '/delete-task/',
    data: {'id': id_task}
  });
}

function doTask(id_task) {
  var date = getDate();
  date['id'] = id_task;
  $.ajax({
    type: 'POST',
    url: '/do-task/',
    data: date,
    success: function (result) {
      if (result['content'] != "") {
        addTask($('#done-tasks  .list-group'), id_task, result['content'], done_tag);
        changed_stats = true;
      }
    }
  });
}

function showTasks(id_task) {
  $.ajax({
    type: 'POST',
    url: '/show-one-task/',
    data: {'id': id_task},
    success: function (result) {
      showOneTask(result['dates']);
    }
  });
}

$(document).on('click', '#task-list .thing button', function () {
  var task = $(this).closest('.thing');
  var id_task = getId(task);
  if ($(this).hasClass('delete')) {
    task.remove();
    deleteTask(id_task);
  }
  else if ($(this).hasClass('show')) {
    $('#refresh').removeAttr('disabled');
    var thing_body = $(this).closest('.thing');
    if ($(this).hasClass('active')) {
      now_showed_one = false;
      $(this).removeClass('active');
      thing_body.removeClass('showed');
      showStatistic();
    }
    else {
      now_showed_one = true;
      $('#refresh').attr('disabled', 'disabled');
      $('.thing.showed').each(function () {
        $(this).removeClass('showed');
        $(this).find('.active').removeClass('active');
      });
      $(this).addClass('active');
      thing_body.addClass('showed');
      showTasks(id_task);
    }
  }
  else { // complete
    doTask(id_task);
  }
});

$(document).on('click', '#done-tasks .thing button', function () {
  changed_stats = true;
  var date = getDate();
  var task = $(this).closest('.thing');
  var id_task = getId(task);
  date['id'] = id_task;
  $.ajax({
    type: 'POST',
    url: '/cancel-task/',
    data: date,
    success: function () {
      task.remove();
      addInfo($('#done-tasks'), no_completed);
    }
  });
});

$(document).ready(function () {
  $('#task-list .thing .text').each(function () {
    $(this).html(linkify($(this).html()));
    ++count_tasks;
  });

  setInterval(function () {
    if (changed_stats && !now_showed_one) {
      calcStatistic(true);
    }
  }, 5000);
});