function prepareText(inputText) {
  var replacedText;
  replacedText = inputText.replace(/</gim, "&lt;");
  replacedText = replacedText.replace(/>/gim, "&gt;");
  return replacedText;
}

function linkify(inputText) {
  var replacedText, replacePattern1, replacePattern2, replacePattern3;

  replacedText = prepareText(inputText);

  //URLs starting with http://, https://, or ftp://
  replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
  replacedText = replacedText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

  //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
  replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

  //Change email addresses to mailto:: links.
  replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
  replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

  return replacedText;
}

function normalizeText(inputText) {
  var text = inputText.split('\n');
  var result = [];
  for (var i = 0; i < text.length; ++i) {
    var paragraph = text[i];
    paragraph.trim();
    if (paragraph != '' || (result.length && result[result.length - 1] != '')) {
      result.push(normalizeLine(paragraph));
    }
  }
  return result.join('\n').trim();
}

function normalizeLine(inputText) {
  var text = inputText.split(' ');
  var result = [];
  for (var i = 0; i < text.length; ++i) {
    if (text[i] != '') {
      result.push(text[i]);
    }
  }
  return result.join(' ');
}