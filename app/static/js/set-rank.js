$(document).ready(function () {
  $('.change-rank li a').click(function (e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
      return;
    }
    var post = getPostBody($(this));
    var id = getPostId(post);
    var type = $(this).html();
    var rank = parseInt($(this).attr('href'));
    changeRank(id, rank);
    $(post).find('.dropdown-menu li').each(function () {
      $(this).removeAttr('class');
    });
    $(this).parent().attr('class', 'active');
    var txt = type + ' <span class="caret"></span>';
    post.find('.dropdown-toggle').html(txt);
  });
});

function changeRank(id, rank) {
  $.ajax({
    type: 'POST',
    url: '/change-rank/',
    data: { 'id': id, 'rank': rank }
  });
}

function getPostBody(element) {
  return element.closest('.post-container');
}

function getPostId(post_body) {
  return post_body.attr('id').split('-')[1];
}

$(document).ready(function () {
  $('.change-rank').each(function () {
    var btn = $(this).find('button');
    var title = $(this).find('.active').children().html();
    btn.html(title + ' <span class="caret"></span>');
  });
});