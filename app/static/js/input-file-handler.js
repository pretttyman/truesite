var MAX_LEN = 25;
var DEFAULT_VALUE = "Choose an image";

$(document).on('change', '.inputfile input', function () {
  var label = $(this).next();
  if (this.files && this.files.length) {
    label.html(cutString(this.files[0].name, MAX_LEN));
  }
  else {
    label.html(DEFAULT_VALUE);
  }
});

function cutString(str, max_len) {
  if (str.length > max_len) {
    str = str.substr(0, max_len) + '...';
  }
  return str;
}