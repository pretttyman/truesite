var MAX_CNT = 99999;

$(document).on('click', '.what-show button', function () {
  var cnt = parseInt($('#count-days').val());
  if (!cnt || cnt > MAX_CNT) {
    cnt = -1;
  }
  cnt = Math.max(cnt, 0);
  var rank = $(this).attr('id').split('-')[1];
  var url = '/show-posts/' + rank + '/' + cnt + '/';
  window.location.replace(url);
});

$(document).ready(function () {
  var btns = $('.rank-btns button');
  for (var i = btns.length - 1; i >= 0; --i) {
    var btn = $(btns[i]);
    if (btn.hasClass('active')) {
      break;
    }
    btn.addClass('active');
  }
});