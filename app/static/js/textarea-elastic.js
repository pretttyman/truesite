var DIFFERENCE = 12; // It's magic const, that decrease textarea height

function resize(textarea) {
  $(textarea).css({'height': 'auto', 'overflow-y': 'hidden'}).height(textarea.scrollHeight - DIFFERENCE);
}

$(document).on('input', 'textarea', function () {
  resize(this);
});