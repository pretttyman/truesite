(function () {

  var app = {

    initialize: function () {
      this.setUpListeners();
    },

    setUpListeners: function () {
      $('form').on('submit', app.submitForm);
    },

    submitForm: function (e) {
      e.preventDefault();

      var form = $(this),
        submitButton = form.find('button[type="submit"]');
      submitButton.attr({disabled: 'disabled'});
      app.validateForm(form);
    },

    validateForm: function (form) {

      var data = form.serialize();

      $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: data,
        success: function (result) {
          var valid = true;
          for (var key in result) {
            if (result[key] == "") {
              $('#' + key).collapse('hide');
            }
            else {
              valid = false;
              $('#' + key).find('p').text(result[key]);
              $('#' + key).collapse({
                toggle: false
              }).collapse('show');
            }
          }
          if (valid) {
            app.validForm();
          }
          else {
            var submitButton = form.find('button[type="submit"]');
            submitButton.removeAttr('disabled');
          }
        }
      });
    },

    validForm: function () {
      window.location.replace('/');
    }

  };

  app.initialize();

}());