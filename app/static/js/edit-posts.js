var button_tag = '<div class="extra" style="text-align: right;"><button type="button" class="btn btn-default btn-xs delete-img">Delete image</button></div>';
var textarea_tag = '<textarea name="edit-post" class="form-control form-inside extra" rows="1" style="resize: none;"></textarea>';
var img_tag = '<div class="img part"></div>';
var text_tag = '<div class="content part"></div>';

var upload_tag = '<form class="extra" enctype="multipart/form-data" style="text-align: right;"> \
                    <div class="inline-block inputfile extra"> \
                      <input type="file" class="new-img" name="img"> \
                      <label class="btn btn-default btn-xs" for="img">Choose an image</label> \
                    </div> \
                  </form>';

$(document).ready(function () {
  $('.content').each(function () {
    $(this).html(linkify($(this).html()));
  });
});

$(document).on('click', '.edit', function () {
  var type = $(this).val();
  if (type == 'Edit') {
    startEdit($(this));
  }
  else {
    finishEdit($(this));
  }
});

function startEdit(button) {
  button.val('Save');
  var post_body = getPostBody(button);
  var text = post_body.find('.content');
  var img_div = post_body.find('.img');
  var img = img_div.find('img');

  if (text.size() == 0) {
    text = $(text_tag);
    post_body.find('.part').first().after(text);
  }
  text.html("");

  if (img_div.size() == 0) {
    img_div = $(img_tag);
    text.after(img_div);
  }
  if (img.size()) {
    img_div.prepend(button_tag);
  }
  else {
    var inp_file = $(upload_tag);
    var id = 'inp-' + getPostId(post_body);
    img_div.prepend(inp_file);
    inp_file.find('input').attr('id', id);
    inp_file.find('label').attr('for', id);
  }

  var container = $(textarea_tag);
  text.append(container);
  post_body.ready(function () {
    container.outerWidth(text.outerWidth());
    container.val(text.attr('data-text'));
    resize(container.get(0));
  });
}

function finishEdit(button) {
  button.val('Edit');
  var post_body = getPostBody(button);
  var id = getPostId(post_body);
  var content = post_body.find('textarea').val();

  button.attr({disabled: 'disabled'});

  var img = null;
  var inp_form = post_body.find('form');
  var files = post_body.find('.new-img').prop('files');
  if (files && files[0]) {
    img = inp_form[0];   // element of form
    insertImage(post_body, files[0]);
    uploadImg(id, img);
  }
  else if (post_body.find('img').length == 0) {
    post_body.find('.img').remove();
  }

  editPost(id, content, post_body, button);
}

$(document).on('click', '.delete', function () {
  var post_body = getPostBody($(this));
  var id = getPostId(post_body);
  deletePost(id);
  post_body.remove();
});

$(document).on('click', '.delete-img', function () {
  var post_body = getPostBody($(this));
  var id = getPostId(post_body);
  deleteImg(id);
  post_body.find('img').remove();
  var btn = post_body.find('.delete-img').parent();
  var inp_file = $(upload_tag);
  btn.after(inp_file);
  inp_file.find('input').attr('id', id);
  inp_file.find('label').attr('for', id);
  btn.remove();
});

function editPost(id_post, content, post_body, button) {
  var data = {'content': content, 'id': id_post};

  console.log(content);
  content = normalizeText(content);
  console.log(content);

  removeExtra(post_body);
  var text = post_body.find('.content');
  if (content == '') {
    text.remove();
  }
  else {
    text.attr('data-text', content);
    content = linkify(content);
    text.html(content);
  }                                                       // else leave original entry
  button.removeAttr('disabled');
  $.ajax({
    type: 'POST',
    url: '/edit-post/',
    data: data,
    success: function (result) {
      //content = result['content'];
    }
  });
}

function uploadImg(id_post, img) {
  var sent_data = new FormData(img);
  sent_data.append('id', id_post);
  $.ajax({
    type: 'POST',
    url: '/assign-img/',
    data: sent_data,
    processData: false,
    contentType: false
  });
}

function deletePost(id_post) {
  $.ajax({
    type: 'POST',
    url: '/delete-post/',
    data: {'id': id_post}
  });
}

function removeExtra(post_body) {
  post_body.find('.extra').remove();
}

function getPostBody(element) {
  return element.closest('.post-container');
}

function getPostId(post_body) {
  return parseInt(post_body.attr('id').split('-')[1]);
}

function deleteImg(id_img) {
  $.ajax({
    type: 'POST',
    url: '/delete-img/',
    data: {'id': id_img}
  })
}

function insertImage(post_body, img) {
  var reader = new FileReader();
  reader.onload = function (e) {
    post_body.find('img').attr('src', e.target.result);
  };

  var tag = '<img src="" class="center">';
  post_body.find('.img').append(tag);
  post_body.ready(function () {
    reader.readAsDataURL(img);
  });
}

$(document).on('submit', '#new-post', function (event) {
  event.preventDefault();
  $(this['content']).val(normalizeText($(this['content']).val()));
  $(this).submit();
});