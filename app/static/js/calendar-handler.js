var MONTHS = ['', 'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December'];
var SIZE = 30; // height and width of cell in calendar
var DONE_MAX_TASKS = 10;

function daysInMonth(month, year) {
  return new Date(year, month, 0).getDate();
}

function convert(day, month, year) {
  return year * 600 + month * 35 + day;
}

function activeDate(day, month, year) {
  var cur = convert(day, month, year);
  return int_start_date <= cur && cur <= int_end_date;
}

function newCurDay(day) {
  day.removeClass('active-day');
  day.addClass('current');
  cur_day.removeClass('current');
  cur_day.addClass('active-day');
  cur_day = day;
}

function paintMonth(calendar, month, year, indent) {
  var title = addMonth(calendar, MONTHS[month] + '  ' + year);
  var width = title.outerWidth();

  var day_in_month = daysInMonth(month, year);
  for (var day = 1; day <= day_in_month; ++day) {
    var data = year + '-' + pad(month) + '-' + pad(day);
    var cur = addDay(calendar, day, data);
    if (activeDate(day, month, year)) {
      cur.addClass('active-day');
      cur_day = cur;
    }
    else {
      cur.addClass('past');
    }
    if (day == 1) {
      if (width <= indent * SIZE) {
        title.css({'margin-right': indent * SIZE - width});
      }
      else {
        title.css({'margin-right': 7 * width});
        cur.css({'margin-left': indent * SIZE});
      }
    }
  }
  return (indent + daysInMonth(month, year)) % 7;
}

function addMonth(calendar, element) {
  var month = $('<div class="in-calendar cell-month">' + element + '</div>');
  calendar.append(month);
  var width = month.outerWidth();
  width = Math.ceil((width + SIZE / 3) / SIZE) * SIZE;
  month.outerWidth(width);
  return month;
}

function addDay(calendar, element, date) {
  var day = $('<div class="in-calendar cell-day">' + element + '</div>');
  calendar.append(day);
  day.attr('data-date', date);
  return day;
}

var start_date = null;
var end_date = null;

var int_start_date;
var int_end_date;

var cur_day;

function getDates(calendar) {
  $.ajax({
    url: '/get-first-date/',
    data: {},
    dataType: 'json',
    success: function (date) {
      start_date = date['start'];
      end_date = date['end'];
      int_start_date = convert(start_date['day'], start_date['month'], start_date['year']);
      int_end_date = convert(end_date['day'], end_date['month'], end_date['year']);
      paintCalendar(calendar);
      showStatistic();
      cur_day.addClass('current');
      cur_day.removeClass('active-day');
      calendar.scrollTop(calendar.prop('scrollHeight'));
      getDoneTasks(end_date);
      setDate(end_date);
    }
  });
}

function paintCalendar(calendar) {
  var day_of_week = new Date(start_date['year'], start_date['month'] - 1, 1).getDay();
  var indent = (6 + day_of_week) % 7;
  for (var year = start_date['year']; year <= end_date['year']; ++year) {
    var month = 0;
    var last_month = 11;
    if (year == start_date['year']) {
      month = start_date['month'];
    }
    if (year == end_date['year']) {
      last_month = end_date['month'];
    }
    for (; month <= last_month; ++month) {
      indent = paintMonth(calendar, month, year, indent);
      if (year != end_date['year'] || month != end_date['month']) {
        calendar.append('<br><br>');
      }
    }
  }
}

function getId(elem) {
  return parseInt(elem.attr('id').split('-')[1]);
}

function makeDisabled() {
  $('#task-menu button').each(function () {
    $(this).attr({disabled: 'disabled'});
  });
}

function pad(num) {
  num = parseInt(num);
  return (num < 10 ? "0" + num : num);
}

function makeDate(date) {
  return date['year'] + '-' + pad(date['month']) + '-' + pad(date['day']);
}

function parseDate(str) {
  var components = str.split('-');
  var day = parseInt(components[2]);
  var month = parseInt(components[1]);
  var year = parseInt(components[0]);
  return {'day': day, 'month': month, 'year': year};
}

$(document).ready(function () {
  var calendar = $('#calendar-container');
  getDates(calendar);

  makeDisabled();

  $(document).on('click', '#task-list .list-group-item', function () {
    $('#task-list .list-group-item').each(function () {
      $(this).removeClass('active');
    });
    $(this).addClass('active');
    $('#task-menu button').each(function () {
      $(this).removeAttr('disabled');
    });
  });

  $(document).on('click', '.cell-day.active-day', function () {
    var date = parseDate($(this).attr('data-date'));
    setDate(date);
    getDoneTasks(date);
    newCurDay($(this));
  });

});

function setDate(date) {
  var str = date['day'] + ' ' + MONTHS[date['month']] + ' ' + date['year'] + ':';
  $('#chosen-date strong').html(str).attr('data-date', makeDate(date));
}

function getDate() {
  var str = $('#chosen-date strong').attr('data-date');
  return parseDate(str);
}

function showStatistic() {
  if (changed_stats) {
    calcStatistic(true);
    return;
  }
  var ind = 0;
  $($('.cell-day').get().reverse()).each(function () {
    if (!$(this).hasClass('past')) {
      var cell = $(this);
      deleteClass(cell);
      var cnt = percentage[ind++];
      cell.addClass('completed-' + cnt);
    }
  });
}

function showOneTask(dates) {
  var ind = 0;
  dates.reverse();
  dates.push('');
  $($('.cell-day').get().reverse()).each(function () {
    if (!$(this).hasClass('past')) {
      var cell = $(this);
      deleteClass(cell);
      if (cell.attr('data-date') === dates[ind]) {
        cell.addClass('completed-' + DONE_MAX_TASKS);
        ++ind;
      }
    }
  });
}

var percentage = null;
var changed_stats = true;
var now_showed_one = false;

function calcStatistic(show) {
  changed_stats = false;
  $.ajax({
    url: '/get-statistics/',
    success: function (result) {
      percentage = result['stats'];
      percentage.reverse();
      percentage.push(0); // if suddenly new day came
      if (show) {
        showStatistic();
      }
    }
  });
}

function deleteClass(cell) {
  var list = cell.attr('class').split(' ');
  for (var i = 0; i < list.length; ++i) {
    if (list[i].split('-')[0] == 'completed') {
      cell.removeClass(list[i]);
      break;
    }
  }
}