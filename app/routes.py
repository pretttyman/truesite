from app import app, db, lm
from flask import render_template, redirect, jsonify, request, url_for, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from .scripts import register_validate, login_validate, get_posts
from .scripts import delete_image, save_image, allowed_file, get_tasks
from .scripts import get_done, get_count, get_date, get_percentage, get_quote
from .models import User, Post, Task, Date, Info, Count
from datetime import date, timedelta
from config import STYLES, RANKS, get_pony


@app.before_request
def before_request():
    g.user = current_user


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/')
@app.route('/posts/')
@login_required
def posts():
    posts = get_posts(g.user)
    return render_template('posts.html',
                           title='Your posts',
                           posts=posts,
                           rank=0,
                           count_days='',
                           ranks=RANKS,
                           styles=[STYLES['posts']],
                           image=get_pony())


@app.route('/register/', methods=['GET', 'POST'])
def register():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('posts'))

    if request.method == 'GET':
        return render_template('register.html',
                               title='Sign Up',
                               styles=[STYLES['log-reg']],
                               image=get_pony())

    # else POST
    username = request.form.get('username')
    email = request.form.get('email')
    password = request.form.get('password')
    another_password = request.form.get('anotherPassword')

    response, valid = register_validate(
            username,
            email,
            password,
            another_password
    )

    if valid:
        new_user = User(username, email, password)
        db.session.add(new_user)
        new_user = User.query.filter_by(username=username).first()
        info = Info(new_user)
        db.session.add(info)
        count_obj = Count(new_user, info.registration_date)
        db.session.add(count_obj)
        db.session.commit()
        login_user(new_user)

    return jsonify(response)


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('posts'))

    if request.method == 'GET':
        return render_template('login.html',
                               title='Sign In',
                               styles=[STYLES['log-reg']],
                               image=get_pony())

    # else POST
    username = request.form.get('username')
    password = request.form.get('password')

    response, valid, user = login_validate(username, password)

    if valid:
        login_user(user, remember=True)

    return jsonify(response)


@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/send-post/', methods=['POST'])
@login_required
def send_post():
    content = request.form.get('content')
    img = request.files.get('img')
    filename = img.filename.lower()
    if content != "" or allowed_file(filename):
        post = Post(g.user, content)
        db.session.add(post)
        db.session.commit()
        if allowed_file(filename):
            post = Post.query.filter_by(id_user=g.user.id).order_by(Post.timestamp.desc()).first()
            save_image(img, post.id)

    return redirect(url_for('posts'))


@app.route('/edit-post/', methods=['POST'])
@login_required
def edit_post():
    content = request.form.get('content')
    post_id = request.form.get('id')
    post = Post.query.filter_by(id=post_id).first()
    if post.id_user == g.user.get_id():
        post.content = content
        db.session.commit()

    return jsonify({'content': content})


@app.route('/delete-post/', methods=['POST'])
@login_required
def delete_post():
    post_id = request.form.get('id')
    post = Post.query.filter_by(id=post_id).first()
    if post.id_user == g.user.get_id():
        if post.img:
            delete_image(post.img)
        db.session.delete(post)
        db.session.commit()

    return jsonify()


@app.route('/show-posts/<rank>/<days>/')
@login_required
def show_posts(rank, days):
    rank = int(rank)
    days = int(days)
    posts = get_posts(g.user, rank, days)
    if days == 0:
        days = ''
    return render_template('posts.html',
                           title='Your posts',
                           posts=posts,
                           rank=rank,
                           count_days=days,
                           ranks=RANKS,
                           styles=[STYLES['posts']],
                           image=get_pony())


@app.route('/change-rank/', methods=['POST'])
@login_required
def change_rank():
    post_id = request.form.get('id')
    rank = request.form.get('rank')
    post = Post.query.filter_by(id=post_id).first()
    if post.id_user == g.user.get_id():
        post.rank = rank
        db.session.commit()

    return jsonify()


@app.route('/delete-img/', methods=['POST'])
@login_required
def delete_post_img():
    post_id = request.form.get('id')
    post = Post.query.filter_by(id=post_id).first()
    if post.id_user == g.user.get_id() and post.img:
        delete_image(post.img)
        post.img = None
        db.session.commit()

    return jsonify()


@app.route('/assign-img/', methods=['POST'])
@login_required
def assign_post_img():
    img = request.files.get('img')
    post_id = request.form.get('id')
    filename = img.filename.lower()
    if allowed_file(filename):
        save_image(img, post_id)

    return jsonify()


@app.route('/calendar/')
@login_required
def calendar():
    tasks = get_tasks(g.user)
    return render_template('calendar.html',
                           tasks=tasks,
                           title='Calendar of tasks',
                           styles=[STYLES['calendar']],
                           image=get_pony())


@app.route('/add-task/', methods=['POST'])
@login_required
def add_task():
    task_body = request.form.get('task')
    task = Task.query.filter(Task.content == task_body, Task.id_user == g.user.get_id()).first()

    if task_body == '' or (task and task.is_active):
        return jsonify({'task': ''})

    if task and task.is_active is False:
        task.is_active = True
    else:
        task = Task(g.user, task_body)
        db.session.add(task)
    count_obj = get_count(g.user, date.today())
    count_obj.count_tasks += 1
    db.session.commit()
    task_id = Task.query.filter_by(content=task_body).first().id
    return jsonify({'id': task_id, 'task': task_body})


@app.route('/delete-task/', methods=['POST'])
@login_required
def delete_task():
    id_task = request.form.get('id')
    task = Task.query.filter_by(id=id_task).first()
    if task.id_user == g.user.get_id():
        # task.dates = []
        if task.count == 0:
            db.session.delete(task)
        else:
            task.is_active = False
        get_count(g.user, date.today()).count_tasks -= 1
        db.session.commit()

    return jsonify()


@app.route('/get-first-date/')
@login_required
def get_first_date():
    fst = g.user.info.registration_date
    start = {'day': fst.day, 'month': fst.month, 'year': fst.year}
    td = date.today()
    end = {'day': td.day, 'month': td.month, 'year': td.year}
    return jsonify({'start': start, 'end': end})


@app.route('/do-task/', methods=['POST'])
@login_required
def do_task():
    task_id = request.form.get('id')
    day = int(request.form.get('day'))
    month = int(request.form.get('month'))
    year = int(request.form.get('year'))
    cur_date = date(year, month, day)
    task = Task.query.filter_by(id=task_id).first()
    if task.dates.filter_by(date=cur_date).count() == 0:
        task.dates.append(get_date(cur_date))
        task.count += 1
        get_count(g.user, cur_date).count_done += 1
        db.session.commit()
        return jsonify({'content': task.content})

    return jsonify({'content': ""})


@app.route('/cancel-task/', methods=['POST'])
@login_required
def cancel_task():
    task_id = request.form.get('id')
    day = int(request.form.get('day'))
    month = int(request.form.get('month'))
    year = int(request.form.get('year'))
    cur_date = date(year, month, day)
    task = Task.query.filter_by(id=task_id).first()
    date_obj = task.dates.filter_by(date=cur_date).first()
    if date_obj:
        get_count(g.user, cur_date).count_done -= 1
        task.count -= 1
        task.dates.remove(date_obj)
        db.session.commit()

    return jsonify({})


@app.route('/get-done-tasks/', methods=['POST'])
@login_required
def get_done_tasks():
    day = int(request.form.get('day'))
    month = int(request.form.get('month'))
    year = int(request.form.get('year'))
    cur_date = date(year, month, day)
    tasks = get_done(g.user, cur_date)

    return jsonify({'tasks': tasks})


@app.route('/get-count-tasks/', methods=['POST'])
@login_required
def get_count_tasks():
    day = int(request.form.get('day'))
    month = int(request.form.get('month'))
    year = int(request.form.get('year'))
    cur_date = date(year, month, day)

    count = get_percentage(get_count(g.user, cur_date))
    return jsonify({'count': count})


@app.route('/get-statistics/')
@login_required
def get_statistics():
    cur_date = g.user.info.registration_date
    td = date.today()
    res = []
    while cur_date <= td:
        res.append(get_percentage(get_count(g.user, cur_date)))
        cur_date += timedelta(days=1)

    return jsonify({'stats': res})


@app.route('/show-one-task/', methods=['POST'])
@login_required
def show_one_task():
    task_id = request.form.get('id')
    task = Task.query.filter_by(id=task_id).first()
    dates = task.dates.order_by(Date.date.asc()).all()
    res = [str(cur_date.date) for cur_date in dates]

    return jsonify({'dates': res})


@app.route('/get-quote/')
def show_quote():
    return jsonify(get_quote())
