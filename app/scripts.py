from validate_email import validate_email
from usernames import is_safe_username
from .models import User, Post, Date, Count, Quote
from passlib.hash import pbkdf2_sha256
from config import UPLOAD_FOLDER, ALLOWED_EXTENSIONS, DONE_ALL_CNT
from app import db
from datetime import timedelta, datetime
from sqlalchemy import func
import os


def register_validate(username, email, password, another_password):
    result = {'errorUsername': "",
              'errorEmail': "",
              'errorPassword': ""}
    valid = True

    # check username
    if len(username) <= 3 or len(username) >= 81:
        result['errorUsername'] = 'Username length must be between 4 and 81 characters'
        valid = False
    elif not is_safe_username(username):
        result['errorUsername'] = 'Invalid username'
        valid = False
    elif not User.query.filter(func.lower(User.username) == username.lower()).first() is None:
        result['errorUsername'] = 'The username is already used'
        valid = False

    # check email
    if not validate_email(email):
        result['errorEmail'] = 'Invalid email'
        valid = False
    elif not User.query.filter_by(email=email.lower()).first() is None:
        result['errorEmail'] = 'The email is already used'
        valid = False

    # check passwords
    if len(password) == 0:
        result['errorPassword'] = 'Password length must be at least 1 character'
        valid = False
    elif password != another_password:
        result['errorPassword'] = """The passwords don't match"""
        valid = False

    return [result, valid]


def login_validate(username, password):
    result = {'errorUsername': ""}
    valid = True
    if validate_email(username):
        cur_user = User.query.filter_by(email=username.lower()).first()
    else:
        cur_user = User.query.filter(func.lower(User.username) == username.lower()).first()

    if cur_user is None or not pbkdf2_sha256.verify(password, cur_user.password):
        result['errorUsername'] = 'Invalid username or password'
        valid = False

    return [result, valid, cur_user]


def get_posts(user, rank=0, days=0):
    if days == 0:
        min_time = datetime.min
    else:
        min_time = datetime.now() - timedelta(days=days)
    posts = user.posts.filter(Post.rank >= rank, Post.timestamp >= min_time).order_by(Post.timestamp.desc()).all()
    result = []
    for post in posts:
        timestamp = post.timestamp.strftime('%d %b %Y at %H:%M')
        cur_post = {'id': post.id,
                    'rank': post.rank,
                    'username': user.username,
                    'timestamp': timestamp,
                    'content': post.content,
                    'img': post.img}
        result.append(cur_post)
    return result


def get_tasks(user):
    tasks = user.tasks.filter_by(is_active=True).all()
    result = []
    for task in tasks:
        cur_task = {'id': task.id,
                    'content': task.content}
        result.append(cur_task)
    return result


def get_done(user, cur_date):
    result = []

    date_obj = Date.query.filter_by(date=cur_date).first()
    if date_obj is None:
        return result

    tasks = date_obj.tasks.filter_by(id_user=user.get_id()).all()
    for task in tasks:
        cur_task = {'id': task.id,
                    'content': task.content}
        result.append(cur_task)
    return result


def delete_image(filename):
    os.remove(os.path.join(UPLOAD_FOLDER, filename))


def save_image(img, post_id):
    filename = str(post_id) + '.' + img.filename.lower().rsplit('.', 1)[1]
    post = Post.query.filter_by(id=post_id).first()
    post.img = filename
    db.session.commit()
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)
    img.save(os.path.join(UPLOAD_FOLDER, filename))


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_count(user, date):
    count_obj = user.count.filter_by(date=date).first()

    if count_obj is None:
        count_obj = Count(user, date)
        prev_date = date - timedelta(days=1)
        count_obj.count_tasks = get_count(user, prev_date).count_tasks
        db.session.add(count_obj)
        db.session.commit()

    return count_obj


def get_percentage(count_obj):
    if count_obj.count_tasks:
        count = min(1, count_obj.count_done / count_obj.count_tasks)
    else:
        count = min(1, count_obj.count_done)
    return round(count * DONE_ALL_CNT)


def get_date(date):
    date_obj = None
    while date_obj is None:
        date_obj = Date.query.filter_by(date=date).first()
        if date_obj is None:
            db.session.add(Date(date))
            db.session.commit()
    return date_obj


def get_quote():
    quote = Quote.query.order_by(func.random()).first()
    return {'text': quote.text, 'author': quote.author}
