from app import db
from passlib.hash import pbkdf2_sha256
from datetime import date, datetime, timedelta


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(81), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password = db.Column(db.String(90))
    posts = db.relationship('Post', backref='user', lazy='dynamic')
    tasks = db.relationship('Task', backref='user', lazy='dynamic')
    info = db.relationship("Info", uselist=False, back_populates="user")
    count = db.relationship('Count', backref='user', lazy='dynamic')

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __init__(self, username, email, password):
        self.username = username
        self.email = email.lower()
        self.password = pbkdf2_sha256.encrypt(password, rounds=10000, salt_size=16)


class Info(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    registration_date = db.Column(db.Date)
    user = db.relationship("User", back_populates="info")

    def __init__(self, user):
        self.id_user = user.id
        self.registration_date = date.today() - timedelta(days=14)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    rank = db.Column(db.Integer, index=True)
    img = db.Column(db.String(20))

    def __init__(self, user, content, rank=0, img=None):
        self.content = content
        self.id_user = user.id
        self.timestamp = datetime.now()
        self.rank = rank
        self.img = img


task_date = db.Table(
    'task_date',
    db.Column('task_id', db.Integer, db.ForeignKey('task.id')),
    db.Column('date_id', db.Integer, db.ForeignKey('date.id')),
)


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text)
    is_active = db.Column(db.Boolean)
    count = db.Column(db.Integer)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    dates = db.relationship('Date', secondary=task_date, backref=db.backref('tasks', lazy='dynamic'), lazy='dynamic')

    def __init__(self, user, content):
        self.content = content
        self.id_user = user.id
        self.is_active = True
        self.count = 0


class Date(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, index=True, unique=True)

    def __init__(self, cur_date):
        self.date = cur_date


class Count(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    date = db.Column(db.Date)
    count_tasks = db.Column(db.Integer)
    count_done = db.Column(db.Integer)

    def __init__(self, user, date):
        self.user_id = user.id
        self.date = date
        self.count_done = 0
        self.count_tasks = 0


class Quote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    author = db.Column(db.String)

    def __init__(self, text, author):
        self.text = text
        self.author = author

'''
Get all post from pavel = User(args):
pavel.posts.filter_by(args).order_by(args).all()
'''
