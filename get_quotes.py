from app.models import db, Quote
from bs4 import BeautifulSoup
import requests

urls = [
    'http://www.brainyquote.com/quotes/topics/topic_motivational{0}.html',
    'http://www.brainyquote.com/quotes/topics/topic_inspirational{0}.html'
]
cnt = [9, 14]

for i, url in enumerate(urls):
    for ind in range(cnt[i]):
        print(ind, end=" ")
        cur_url = url.format('' if ind == 0 else str(ind + 1))
        page = requests.get(cur_url)
        soup = BeautifulSoup(page.text, 'html.parser')
        soup = soup.find('div', {'id': 'quotesList'})
        quotes = soup.findAll('div', {'class': 'boxyPaddingBig'})
        for quote in quotes:
            txt = quote.find('a', {'title': 'view quote'}).text
            author = quote.find('a', {'title': 'view author'}).text
            new_quote = Quote(txt, author)
            db.session.add(new_quote)
    print('')

db.session.commit()
print('finished')
