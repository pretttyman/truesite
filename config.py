import os
from datetime import datetime, timedelta
from random import randint

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

CSRF_ENABLED = True
SECRET_KEY = '8-800-555-35-35 better call, then from somebody borrow'

UPLOAD_FOLDER = os.path.join(basedir, 'app/static/img')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

DONE_ALL_CNT = 10

def delta_time(rank):
    if rank == 0:
        return datetime.min
    elif rank == 1:
        return datetime.utcnow() - timedelta(days=7)
    elif rank == 2:
        return datetime.utcnow() - timedelta(days=30)
    elif rank == 3:
        return datetime.utcnow() - timedelta(days=90)
    elif rank == 4:
        return datetime.utcnow() - timedelta(days=365)
    elif rank == 5:
        return datetime.min


STYLES = {
    'posts': 'css/posts.css',
    'log-reg': 'css/log-reg.css',
    'calendar': 'css/calendar.css'
}

RANKS = {
    0: 'Usual',
    1: 'Interesting',
    2: 'Worthy',
    3: 'Lovely',
    4: 'Amazing',
    5: 'Awesome'
}

folder_img = 'used_img'
IMAGES = [
    'twilight.png',
    'pinkie-pie.png',
    'rainbow-dash.png',
    'fluttershy.png',
    'applejack.png',
    'rarity.png',
    'spike.png'
]


def get_pony():
    return "{0}/{1}".format(folder_img, IMAGES[randint(0, len(IMAGES) - 1)])
